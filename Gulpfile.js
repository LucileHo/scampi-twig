//
// Scampi Twig Gulpfile
//
// ----------------------------------------------------------------------------


// Pour connaître l'ensemble des tâches disponibles :
// ----------------------------------------------------------------------------
//
// $ gulp help
//

var gulp    = require('gulp-help')(require('gulp'));


// Import des taches du repertoire tasks
var clean   = require('./tasks/clean.js');
var build   = require('./tasks/build.js');
var html    = require('./tasks/html.js');
var css     = require('./tasks/css.js');
var script  = require('./tasks/scripts.js'); // à finaliser
var watch   = require('./tasks/watch.js'); // à finaliser avec le js
var svg     = require('./tasks/svg.js');

