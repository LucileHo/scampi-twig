# Scampi-Twig

Outil de création de pages statiques ou de gabarits d’intégration html/css/js utilisant la bibliothèque de composants [Scampi](https://gitlab.com/pidila/scampi) pour les CSS et Twig pour la génération des pages html.

Scampi-Twig autorise la saisie des contenus par inclusion de fragments, en saisie twig/html directe ou par inclusion de fichiers markdown.

Pour l’utiliser, cloner ce dépôt et lire la présente page.


Ajouter le submodule Scampi
------------------------------------------------------------------------------

En principe le clone du dépôt embarque automatiquement le submodule. C'est le cas si vous utilisez SourceTree par exemple. 

Sinon, faire cette suite d'instructions en ligne de commande :

```bash
$ git clone https://gitlab.com/pidila/scampi-twig.git
$ cd scampi-twig/
$ git submodule init
$ git submodule update
```

Il est vivement recommandé de ne pas modifier les fichiers inclus dans ce submodule, les modifs ne seraient appliquées qu’en local et cela compromettrait la possibilité de le mettre à jour.

Si on souhaite apporter une amélioration au submodule Scampi, on le fait [depuis son dépôt](https://gitlab.com/pidila/scampi).


Personnaliser le déploiement des modules node
------------------------------------------------------------------------------

Dans le fichier `package.json`, ajuster le nom, la version et l’url du dépôt ainsi que la licence et les auteurs.

Lancer la commande :

```bash
$ npm install
```

Installer Gulp en global

```bash
$ npm install gulp -g 
```

L’ensemble des taches sont disponibles avec la commande :

```bash
$ gulp help
```


Créer les pages html avec Twig
------------------------------------------------------------------------------

Le fichier `dev/templates/layout/base.html` rassemble les éléments communs à tous les projets.

Le fichier `dev/templates/layout/base-projet.html` hérite de la base.html et ajoute les particularités communes à toutes les pages d’un projet.

Le fichier `dev/templates/pages/index.twig` sert de centre d’aiguillage pour les pages prêtes à être recettées.

Le répertoire *dev/templates/pages/* accueille les pages complètes. Une première page est livrée avec le dépôt pour le styleguide, qui sera à compléter avec les éléments propres au projet.

Des tâches gulp génèrent les pages html dans le répertoire *public* :

  * `gulp make:html` pour compiler


### Prise en charge de markdown

Les contenus en markdown peuvent être insérés de deux façons :

  * en saisie directe dans un template :
  
  ```
  {% markdown %}
    Contenu
  {% endmarkdown %}
  ```

  * par inclusion de fichier depuis un template :
  
  ```
  {% markdown "chemin/relatif/depuis/templates" %}{% endmarkdown %}
  ```


Gérer les fichiers javascript
------------------------------------------------------------------------------

Une tâche gulp prend en charge les scripts externes prérequis (jquery, modernizr) pour les déplacer dans le projet :

`gulp install:js-vendors`

Lorsque vous utilisez un module de scampi comportant un script, recopiez-le dans *dev/_assets/scripts/main/*. Vous pourrez par exemple y copier immédiatement le script du module anchor-focus, qui gère un bug de Chrome/Safari/IE.

Ces scripts seront concaténés et envoyés dans le build, soit par la tâche unitaire, soit par la tâche globale de build.

  * tâche unitaire : `gulp make:js-main`


Compiler les css
------------------------------------------------------------------------------

Un fichier prêt à l’emploi est présent dans *dev/_assets/scss/projet/*. Pour personnaliser un projet :

  1. recopier dans *projet* le fichier core-scampi-settings, le renommer et le personnaliser, l’importer en lieu et place de celui de scampi ;
  2. dans *style.css* décommenter les imports de modules scampi utilisés tels quels ;
  3. ajouter les modules et partials propres au projet.

Deux tâches gulp sont disponibles :

  * `gulp make:css` pour la compilation et le placement du fichier css dans *public/* ;
  * `gulp make:css-min` pour la compilation et la minification.


Traiter les icones svg
------------------------------------------------------------------------------

Si les gabarits comportent des icônes au format svg, elles doivent être placées dans *dev/_assets/project/icones/unitaires/*. À titre d’exemple, 2 icônes y sont déjà placées.

Une tâche gulp permet de générer depuis ce répertoire un sprite d’icones svg à utiliser dans les gabarits :

  * `gulp make:svg` transforme les icones unitaires en sprite *(icon-sprite.svg)* et le place dans *dev/_assets/project/icones/*

Le sprite sera déplacé avec les autres resssources statiques quand on effectuera la commande gulp générale `gulp install:assets`.


Déplacer les fichiers statiques
------------------------------------------------------------------------------

Les éléments du répertoire *dev/_assets/project* (à l’exclusion des scss et des scripts) sont déplacés tels quels dans le répertoire public grâce à des tâches gulp.

  * `gulp install:assets` pour tous les éléments du répertoire _assets/project
  * `gulp install:favicon` ajoute la favicon à la racine du site


Récapitulatif des tâches gulp principales
------------------------------------------------------------------------------

On accède à la liste des tâches gulp disponibles et leur résumé à l'aide de la commande `gulp help`.

  * Tout construire   :   `gulp build`
  * Tout nettoyer     :   `gulp clean`
  * Compiler sass     :   `gulp make:css`
  * Compiler twig     :   `gulp make:html`

Pendant les développements on peut lancer une tâche qui lance un serveur, ouvre la page d’index et surveille les modificationss effectuées sur les css et le html :

  * `gulp live`


Déploiement sur <user>.gitlab.io/<projet>
------------------------------------------------------------------------------

### Sans submodule

Si la bibliothèque Scampi a été ajoutée directement sans utiliser la méthode de submodule, la procédure est simple.

Placer à la racine du dépôt un fichier *.gitlab-ci.yml* avec le contenu suivant :

```yaml
image: node
pages:
  stage: deploy
  script:
  - npm install
  - ./node_modules/.bin/gulp build:public
  artifacts:
    paths:
    - public
  only:
  - master
```

  * image : équipe le package docker qui sera utilisé pour "mouliner" les fichiers avant de les envoyer sur le serveur
  * pages : crée une tâche nommée *pages* qui demande au "runner" d’effectuer sur les fichiers du dépôt les tâches définies par la section *script* puis de déployer des fichiers produits dans un répertoire *public* dès qu’un commit est poussé sur *master*.

On peut surveiller ces opérations dans l’onglet « Pipelines » du dépôt.

Pour plus d’informations, voir [ce tuto pas à pas](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/).

**À noter :** Les pages déployées ne peuvent être protégées par un mot de passe, même si le dépôt est privé. Penser à ajouter des metas attributs noindex nofollow dans le head des pages.

### Avec submodule

Si Scampi a été joint au projet en tant que submodule, c’est un petit peu plus compliqué (mais pas trop !).

Il faut tout d’abord créer une paire de clés ssh. On ajoute la clé publique dans les clés autorisées du projet (soit en créant un utilisateur dédié soit en l’ajoutant aux clés de l’un des membres de l’équipe) et la clé privée dans les variables du dépôt (sur gitlab.com menu settings > Variables) par exemple sous le nom `SSH_PRIVATE_KEY`.

Le fichier *.gitlab-ci.yml* devient alors :

```yaml
image: node

before_script:
   - ’which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )’
   - eval $(ssh-agent -s)
   - ssh-add <(echo "$SSH_PRIVATE_KEY")
   - mkdir -p ~/.ssh
   - ’[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config’
   - git submodule update --init --recursive


pages:
  stage: deploy
  script:
  - npm install
  - ./node_modules/.bin/gulp build-dev
  artifacts:
    paths:
    - public
  only:
  - master
```

Contact
------------------------------------------------------------------------------

Benoît Dequick - Hugues Moreno - Anne Cavalier. Nous écrire : prenom.nom@dila.gouv.fr

### Liste d’échanges et d’information :

[S’abonner](https://framalistes.org/sympa/subscribe/pidila-tools).


Licence
------------------------------------------------------------------------------

Scampi-twig est distribué sous une double licence :[MIT](https://gitlab.com/pidila/scampi-twig/blob/master/LICENCE-MIT.md) et [CeCILL-B](http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html).

Vous pouvez utiliser Scampi avec l’une ou l’autre licence.

La documentation est sous licence [Creative Commons CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/fr/).
